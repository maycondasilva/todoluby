import * as Yup from 'yup';

export default Yup.object().shape({
  title: Yup.string()
  .min(5,'No mínimo 5 dígitos!')
  .required('Título obrigatório!')
});




