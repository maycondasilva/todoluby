import React, { useState, useEffect } from 'react';
import { Container, Title, ContainerTask, Task, Error } from './styles';
import { uuid } from 'uuidv4';
import { Formik, Field, Form, FormikHelpers, ErrorMessage} from 'formik';
import schema from '../../utils/schema'


interface Values {
  title: string;
}

interface TodoList {
  id: string;
  title: string;
}

const TodoList: React.FC = () => {

  const [todoList, setTodoList] = useState<TodoList[]>(() => {
    const storagedTodo = localStorage.getItem(
      '@todoLuby:todo',
    );

    if (storagedTodo) {
      return JSON.parse(storagedTodo);
    }
    return [];
  });

  function onSubmit( values: Values,
    { setSubmitting, resetForm  }: FormikHelpers<Values>) {

      try{
        const newTask = {
          id : uuid(),
          title : values.title
        };
        setTodoList([...todoList, newTask]);
        resetForm({});
      } catch (err) {
        console.log(err.errors);
      }
  }

  useEffect(() => {
    localStorage.setItem('@todoLuby:todo', JSON.stringify(todoList));
  }, [todoList]);

  return (
    <Formik
      validationSchema={schema}
      onSubmit={onSubmit}
      initialValues={{
        title: '',
      }}
      render={({ values, errors, touched  }) => (
        <Container>
          <Form>
            <Title>TodoLuby</Title>
            <Error>
              <ErrorMessage name="title" className="Error"/>
            </Error>
            <Field
              name = "title"
              placeholder="Digite o nome da tarefa"
            />

            <button type="submit">Adicionar</button>
          </Form>

          <Task>
            {todoList.map(task => (
              <ContainerTask>
                <div key={task.id}>
                  <p>{task.title}</p>
                </div>
              </ContainerTask>
            ))}
          </Task>
        </ Container>
      )}
    />
  );
};

export default TodoList;
