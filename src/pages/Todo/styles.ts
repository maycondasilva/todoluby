import styled from 'styled-components';
import { shade } from 'polished';

export const Title = styled.h1`
  font-size: 32px;
  color: #3a3a3a;
  text-align: center;
  margin-bottom: 80px;
`;

export const Container = styled.div`
  margin-top: 40px;
  max-width: 700px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  input {
    flex: 1;
    height: 50px;
    width: 550px;
    padding: 0 24px;
    border: 0;
    border-radius: 5px 0 0 5px;
    color: #3a3a3a;
    border: 2px solid #fff;
    border-right: 0;
    font-size: 18px;
  }

  button {
    width: 150px;
    height: 50px;
    background: #00BFFF;
    border-radius: 0px 5px 5px 0px;
    border: 0;
    color: #fff;
    font-weight: bold;
    transition: background-color 0.2s;
    font-size: 18px;
    &:hover {
      background: ${shade(0.2, '#00BFFF')};
    }
  }
`;

export const Task = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  max-width: 700px;
  align-items: center;
  justify-content: center;
`;

export const ContainerTask = styled.div`
  background: #fff;
  border-radius: 5px;
  width: 100%;
  height: 100%;
  padding: 16px;
  margin-top: 16px;
  text-decoration: none;

  word-break: break-all;
  white-space: break-all;

  div {
    margin: 0 16px;
    flex: 1;

    p {
      font-size: 22px;
      color: black;
      margin-top: 4px;
    }
  }
`;

export const Error = styled.span`
  display: block;
  color: #c53030;
  margin-top: 8px;
`;
