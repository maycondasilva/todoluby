import React from 'react';
import GlobalStyles from './styles/global';
import TodoList from './pages/Todo/';

const App: React.FC = () => (
  <>
    <TodoList />
    <GlobalStyles />
  </>
);

export default App;
