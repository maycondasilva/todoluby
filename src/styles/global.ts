import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;
}
body {
  -webkit-font-smoothing: antialiased;
  background-image: linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%);
}

border-style, -moz-user-input, button {
  font: 16px Roboto, sans-serif,
}

#root {
  max-width:960px;
  margin: 0 auto;
  padding: 40px 20px;
}

button {
  cursor: pointer;
}
`;
